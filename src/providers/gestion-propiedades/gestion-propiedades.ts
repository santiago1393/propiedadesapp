import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { propiedad } from '../../interfaces/propiedad.interface';
import { Storage } from '@ionic/storage';

/*
  Generated class for the GestionPropiedadesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GestionPropiedadesProvider {

  listadoPropiedades:propiedad[] = [];

  constructor( private storage: Storage) {
    console.log('Hello GestionPropiedadesProvider Provider');
  }

  addPropiedad(prop:propiedad){
    this.listadoPropiedades.push(prop);
    let promise = new Promise((resolve,reject)=>{
      this.savePropiedades().then(()=>{
        resolve();
      }).catch((error)=>{
        reject(error);
      });
    });
    return promise;
  }

  getPropiedades(){
    let promise = new Promise((resolve,reject) => {
      this.storage.get("propiedades").then((valor) => {       
        if(valor){
          console.log("Cargadas ok " + JSON.stringify(valor));
          this.listadoPropiedades = valor;            
        }else{
          console.log("Cargadas not Ok " + JSON.stringify(valor));
         this.listadoPropiedades = []
        }        
        resolve();
      }).catch((error)=>{
        console.log("Error al obtener propiedades: " + error);        
        reject(error);        
      });
    });

    return promise;
  }

  savePropiedades(){
    let promise = new Promise((resolve,reject) => {
      this.storage.set("propiedades", this.listadoPropiedades).then(() => {
        console.log("Guardado");
        resolve();
      }).catch((error)=>{
        console.log("Error al guardar" + error);        
        reject(error);        
      });
    });

    return promise;
  }

  deletePropiedad(index:number){    
    this.listadoPropiedades.splice(index,1);
    this.savePropiedades();
  }

}
