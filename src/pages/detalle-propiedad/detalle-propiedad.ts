import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { propiedad } from "../../interfaces/propiedad.interface";

/**
 * Generated class for the DetallePropiedadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-propiedad',
  templateUrl: 'detalle-propiedad.html',
})
export class DetallePropiedadPage {

  propiedadDetalle:propiedad;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.propiedadDetalle = this.navParams.get("propiedad");
    console.log(this.propiedadDetalle.image);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallePropiedadPage');
  }

}
