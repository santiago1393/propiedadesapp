import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallePropiedadPage } from './detalle-propiedad';

@NgModule({
  declarations: [
    DetallePropiedadPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallePropiedadPage),
  ],
})
export class DetallePropiedadPageModule {}
