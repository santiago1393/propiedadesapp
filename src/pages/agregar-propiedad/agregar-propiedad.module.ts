import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgregarPropiedadPage } from './agregar-propiedad';

@NgModule({
  declarations: [
    AgregarPropiedadPage,
  ],
  imports: [
    IonicPageModule.forChild(AgregarPropiedadPage),
  ],
})
export class AgregarPropiedadPageModule {}
