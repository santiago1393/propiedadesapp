import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { propiedad } from "../../interfaces/propiedad.interface";
import { Geolocation } from '@ionic-native/geolocation';
import { GestionPropiedadesProvider } from "../../providers/gestion-propiedades/gestion-propiedades";
import { ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
/**
 * Generated class for the AgregarPropiedadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregar-propiedad',
  templateUrl: 'agregar-propiedad.html',
})
export class AgregarPropiedadPage {

  
  celular:number;
  direccion:string;
  descripcion:string;
  telefono:number;
  latitud:number;
  longitud:number;
  image:string;

  imagenPreview:string = "";
  imagen64:string;
  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              private imagePicker: ImagePicker,
              private camera:Camera,
              public gestion:GestionPropiedadesProvider,
              public navParams: NavParams,
              public geolocation:Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgregarPropiedadPage');
  }

  select_photo(){
    let opciones:ImagePickerOptions = {
      quality:70,
      outputType:1,
      maximumImagesCount:1
    }
    this.imagePicker.getPictures(opciones).then((results) => {
      for (var i = 0; i < results.length; i++) {
          this.imagenPreview = 'data:image/jpg;base64,' +  results[i];
          this.imagen64 = results[i];
         // console.log('Image URI: ' + results[i]);
      }
    }, (err) => {
      console.log("Error en seleccionar: " + JSON.stringify(err));
     });
  }

  mostrar_camara(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.imagenPreview = 'data:image/jpg;base64,' + imageData;
     this.imagen64 = imageData;
    }, (err) => {
     // Handle error
     console.log("Error en Camara: " + JSON.stringify(err));
    });
  }

  agregar(){
    let prop:propiedad = {
      codigo: new Date().valueOf().toString(),
      descripcion : this.descripcion,
      celular: this.celular,
      telefono:this.telefono,
      direccion:this.direccion,
      image:this.imagenPreview,
      ubicacion:{
        latitud:this.latitud,
        longitud:this.longitud
      }
    };
    console.log(prop.celular);
    console.log(prop.direccion);
    if(prop.celular !== undefined && prop.direccion !== undefined && prop.descripcion !== undefined){
      this.gestion.addPropiedad(prop).then(()=>{
        console.log("Propiedad Agregada");
        this.presentToast("Agregada correctamente");
        this.navCtrl.pop();
      }).catch((error)=>{
        this.presentToast("Error: " + JSON.stringify(error));
        console.log("Error: " + error);
      });
    }else{
      this.presentToast("Debes completar la información minima!");
    }  
  }

  get_coords(){
      
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  presentToast(mss:string) {
    const toast = this.toastCtrl.create({
      message: mss,
      duration: 3000
    });
    toast.present();
  }
}
