import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { propiedad } from '../../interfaces/propiedad.interface';

import { AgregarPropiedadPage } from "../agregar-propiedad/agregar-propiedad";
import { DetallePropiedadPage } from '../detalle-propiedad/detalle-propiedad';

import { GestionPropiedadesProvider } from "../../providers/gestion-propiedades/gestion-propiedades";
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  listadoPropiedades:propiedad[] = [];

  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public gestion:GestionPropiedadesProvider) { 
    this.cargarPropiedades();
  }

  verDetalle(prop:propiedad){
    console.log(prop);
    this.navCtrl.push(DetallePropiedadPage, {"propiedad": prop})
  }

  addProp(){
    this.navCtrl.push(AgregarPropiedadPage);
  }

  cargarPropiedades(){
    this.gestion.getPropiedades().then(()=>{
      console.log(this.gestion.listadoPropiedades); 
      this.listadoPropiedades = this.gestion.listadoPropiedades;
    }).catch(()=>{
      console.log("No se cargaron");
      this.listadoPropiedades = [];
    })
  }

  borrar(cod:number){
    console.log("Borrar: " + cod);    
    this.gestion.deletePropiedad(cod);
    this.presentToast("Borrado correctamente!");
  }  
  
  presentToast(mss:string) {
    const toast = this.toastCtrl.create({
      message: mss,
      duration: 3000
    });
    toast.present();
  }
}
