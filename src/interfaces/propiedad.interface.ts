export interface propiedad{
    codigo:string;
    direccion:string;
    telefono:number;
    celular:number;
    image:string;
    descripcion:string;
    ubicacion:{
        longitud:number;
        latitud:number
    };    
}